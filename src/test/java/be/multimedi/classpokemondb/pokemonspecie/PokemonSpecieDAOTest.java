package be.multimedi.classpokemondb.pokemonspecie;

import be.multimedi.classpokemondb.db.DriverManagerWrapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PokemonSpecieDAOTest {
    @BeforeAll
    public void beforeAllTests(){
        DriverManagerWrapper.url="jdbc:mariadb://javaeegenk.be/javaeegenkDB4";
    }
    @AfterAll
    public void afterAllTests(){
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB5";
    }

@Test
public void getPokemonSpecieByIdTest(){

    PokemonSpecie pokemonSpecie = PokemonSpecieDAO.getPokemonSpById(1);
   assertNotNull(pokemonSpecie);
    assertEquals(Integer.valueOf(1), pokemonSpecie.getId());
    assertEquals( "Bulbasaur", pokemonSpecie.getName());
    assertEquals(Integer.valueOf(5),pokemonSpecie.getType1Id());
    assertEquals(Integer.valueOf(8),pokemonSpecie.getType2Id());
    assertNull(pokemonSpecie.getEvolvedFromId());//this line ???ask Ward

}

@Test
public void getPokemonSpecieByNameTest(){

    PokemonSpecie pokemonSpecie = PokemonSpecieDAO.getPokemonSpecieByName("Ivysasaur");
    assertNotNull(pokemonSpecie);
    assertEquals(Integer.valueOf(2), pokemonSpecie.getId());
    assertEquals( "Ivysasaur", pokemonSpecie.getName());
    assertEquals(Integer.valueOf(5),pokemonSpecie.getType1Id());
    assertEquals(Integer.valueOf(8),pokemonSpecie.getType2Id());
    assertEquals(Integer.valueOf(1),pokemonSpecie.getEvolvedFromId());
  //  assertEquals(Integer.valueOf(1),list.forEach(list->););

}

  @Test
    public void getAllPokemonSps(){
        List<PokemonSpecie> list = PokemonSpecieDAO.getAllPokemonSpecies();
        assertTrue(list.size() > 0);
    }



}
