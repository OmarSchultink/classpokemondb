package be.multimedi.classpokemondb.pokemonspecie;

import be.multimedi.classpokemondb.db.DriverManagerWrapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PokemonSpecieTest {
   // @BeforeAll
 /*   public void beforeAllTests(){
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB4";
    }*/
    @Test
    public void createPokemonSpTest(){
        PokemonSpecie pokemonSp = new PokemonSpecie("Pikachu",1,1,1);
        assertEquals(Integer.valueOf(-1), pokemonSp.getId());
        assertEquals("Pikachu", pokemonSp.getName());
        assertEquals(Integer.valueOf(1), pokemonSp.getType1Id());
        assertEquals(Integer.valueOf(1), pokemonSp.getType2Id());
        assertEquals(Integer.valueOf(1), pokemonSp.getEvolvedFromId());


    }

    @AfterAll
    public void afterAllTests(){
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB5";
    }

}
