package be.multimedi.classpokemondb.trainer;

import be.multimedi.classpokemondb.db.DriverManagerWrapper;
import org.junit.jupiter.api.*;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;

import static be.multimedi.classpokemondb.db.DriverManagerWrapper.*;
import static org.junit.jupiter.api.Assertions.*;



@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TrainerDAOTest {


    static @BeforeAll
    public void beforeAllTests(){
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB4";

    }
    static @AfterAll
    public void afterAllTests(){
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB5";
    }

    @BeforeEach
    public void beforeEachTest(){
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             final Statement stmt = con.createStatement();
             final Statement stmt3 = con.createStatement();
             final Statement stmt2 = con.createStatement()){
            stmt.execute("DELETE FROM Trainer WHERE id >0 ");
            stmt3.execute("ALTER Table Trainer AUTO_INCREMENT = 1");
            stmt2.execute("INSERT INTO Trainer (firstName, lastName, birthday, city) VALUES ('Ash', 'Ketchum', '1985-01-01', 'Pellet Town')");
        } catch (SQLException sqle){
            System.out.println("Oops" + sqle);
        }
    }

    @Test
    public void registerTrainerTest(){
        assertTrue(TrainerDAO.registerTrainer(new Trainer("Ash", "Ketchum", LocalDate.of(1985,1,1), "Pellet Town")));
    }

    @Test
    public void getTrainerByIdTest(){
        Trainer t = TrainerDAO.getTrainerById(1);
        assertNotNull(t);
        assertEquals(1, t.getId());
        assertEquals( "Ash", t.getFirstName());
        assertEquals("Ketchum", t.getLastName());
        assertEquals(LocalDate.of(1985,1,1), t.getBirthday());
        assertEquals("Pellet Town", t.getCity());
    }

    @Test
    public void getAllTrainers(){
        List<Trainer> list = TrainerDAO.getAllTrainers();
        assertTrue(list.size() > 0);
    }

    @Test
    public void registerGetByFirstNameAndRemoveTrainerTest(){
        assertTrue(TrainerDAO.registerTrainer(
                new Trainer("Pipi", "Schultink",
                        LocalDate.of(1981, 9, 12), "Achel")));
        List<Trainer> list = TrainerDAO.getTrainersByFirstName("Pipi");
        Trainer t = list.get(0);
        assertTrue( TrainerDAO.removeTrainer( t ) );
    }

    @Test
    public void alterAll(){
        assertTrue(TrainerDAO.alterTrainerFirstName(1, "Brock"));
        assertTrue(TrainerDAO.alterTrainerLastName(1, "Smith"));
        assertTrue(TrainerDAO.alterTrainerBirthday(1, LocalDate.of(1995,2,6)));
        assertTrue(TrainerDAO.alterTrainerCity(1,"Safron"));
    }

}
