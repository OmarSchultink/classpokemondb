package be.multimedi.classpokemondb.trainer;

import be.multimedi.classpokemondb.db.DriverManagerWrapper;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import java.time.LocalDate;

public class TrainerTest {
    static @BeforeAll
    public void beforeAllTests(){
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB4";
    }
    static @AfterAll
    public void afterAllTests(){
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB5";
    }

    @Test
    public void registerTrainerTest(){
        Trainer t = new Trainer(1,"Ash","Ketchum", LocalDate.of(1990,9,9),"Pallet Town");
        assertEquals(1,t.getId());
        assertEquals("Ash",t.getFirstName());
        assertEquals("Ketchum",t.getLastName());
        assertEquals(LocalDate.of(1990,9,9),t.getBirthday());
        assertEquals("Pallet Town",t.getCity());
    }

}
