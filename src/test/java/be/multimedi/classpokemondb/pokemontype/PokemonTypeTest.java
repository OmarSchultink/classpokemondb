package be.multimedi.classpokemondb.pokemontype;

import be.multimedi.classpokemondb.db.DriverManagerWrapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PokemonTypeTest {
    @BeforeAll
    public void beforeAllTests() {
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB4";
    }

    @AfterAll
    public void afterAllTests() {
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB5";
    }
    @Test
    public void PokemonTypesTest() {
        PokemonType pt = new PokemonType(3, "Fire");
        assertEquals(3,  (int)pt.getId());
        assertEquals("Fire", pt.getName());


        assertThrows(IllegalArgumentException.class, () -> pt.setName(null));
    }

}
