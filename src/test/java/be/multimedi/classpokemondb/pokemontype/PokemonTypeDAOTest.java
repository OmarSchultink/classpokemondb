package be.multimedi.classpokemondb.pokemontype;

import be.multimedi.classpokemondb.db.DriverManagerWrapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)

public class PokemonTypeDAOTest {
    @BeforeAll
    public void beforeAllTests() {
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB4";
    }

    @AfterAll
    public void afterAllTests() {
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB5";
    }


    @Test
    public void getPokemonTypeById() {
        PokemonType pt = PokemonTypeDAO.getPokemonTypeById(1);
        assertNotNull(pt);
        assertEquals(1, (int)pt.getId());
        assertEquals("normal", pt.getName());
    }

    @Test
    public void getPokemonTypeByName() {
        List<PokemonType> list = PokemonTypeDAO.getPokemonTypeByName("grass");
        assertNotNull(list);

        PokemonType pt = list.get(0);
        assertEquals("grass", pt.getName());
        assertEquals(5, (int)pt.getId());

    }

    @Test
    public void getAllPokemonTypes() {
        List<PokemonType> pk = PokemonTypeDAO.getAllPokemonTypes();
        assertTrue(pk.size() >= 15);
    }


}
