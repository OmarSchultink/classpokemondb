package be.multimedi.classpokemondb.pokemon;

import be.multimedi.classpokemondb.db.DriverManagerWrapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PokemonTest {
    @BeforeAll
    public void beforeAllTests(){
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB4";
    }

    @Test
    public void CreatePokemonTest(){
        Pokemon poke = new Pokemon("Pikachu",25,1);
        assertEquals("Pikachu",poke.getName());
        assertEquals(Integer.valueOf(25),poke.getSPID());
        assertEquals(Integer.valueOf(1),poke.getTrainerId());
    }

    @AfterAll
    public void afterAllTests(){
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB5";
    }

}
