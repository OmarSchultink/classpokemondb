package be.multimedi.classpokemondb.pokemon;

import be.multimedi.classpokemondb.db.DriverManagerWrapper;
import org.junit.jupiter.api.*;

import java.sql.*;
import java.util.List;

import static be.multimedi.classpokemondb.db.DriverManagerWrapper.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PokemonDAOTest {
    @BeforeAll
    public static void beforeAllTests() {
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB4";
    }
    @AfterAll
    public static void afterAllTests(){
        DriverManagerWrapper.url = "jdbc:mariadb://javaeegenk.be/javaeegenkDB5";
    }
    @BeforeEach
    public void beforeEachTest(){
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             final Statement stmt = con.createStatement();
        ) {
            stmt.execute("TRUNCATE Table Pokemon");
            System.out.println("truncated");
        }catch(SQLException se){
            System.out.println("oops: " + se);
        }
    }
    @Test
    void RegisterGetByNameRemovePokemon(){
        assertTrue(PokemonDAO.registerPokemon(new Pokemon("Pizza",1,1)));
        List<Pokemon> list = PokemonDAO.getPokemonByName("Pizza");
        Pokemon p = list.get(0);
        assertEquals(Integer.valueOf(1),p.getSPID());
        assertTrue(PokemonDAO.removePokemon(p));
    }
    @Test
    void RegisterAlterAllGetByNameRemovePokemon(){
        assertTrue(PokemonDAO.registerPokemon(new Pokemon("Petoetje2",1,1)));
        assertTrue(PokemonDAO.alterPokemonName(Integer.valueOf(1),null));
        assertTrue(PokemonDAO.alterPokemonName(Integer.valueOf(1),"Petoetje"));
        assertTrue(PokemonDAO.alterPokemonSPId(Integer.valueOf(1),Integer.valueOf(2)));
        List<Pokemon> list = PokemonDAO.getPokemonByName("Petoetje");
        assertTrue(list.size()>0);
        Pokemon p = list.get(0);
        assertEquals("Petoetje",p.getName());
        assertEquals(Integer.valueOf(2),p.getSPID());
        assertTrue(PokemonDAO.removePokemon(p));
    }

    @Test
    void alterTrainerPokemonTrainerId(){
        //pokemon kris is the pokemon of trainer with id=1 (Astrid)
        assertTrue(PokemonDAO.registerPokemon(new Pokemon("kris",1,1)));
        Pokemon kris = PokemonDAO.getPokemonByName("kris").get(0);
        //now we see if we can make pokemon kris without any trainer (set to null)
        assertTrue(PokemonDAO.alterPokemonTrainerId(kris.getId(),null));
        assertNull(PokemonDAO.getPokemonByName("kris").get(0).getTrainerId());
        //no we test if a pokemon with initially no trainer, can be given a trainer
        assertTrue(PokemonDAO.registerPokemon(new Pokemon("steven",5,null)));
        Pokemon steven = PokemonDAO.getPokemonByName("steven").get(0);
        assertNull(steven.getTrainerId());
        //no we give steven a trainer (astrid(id=1))
        assertTrue(PokemonDAO.alterPokemonTrainerId(steven.getId(),1));
        steven=PokemonDAO.getPokemonByName("steven").get(0);
        assertNotNull(steven.getTrainerId());
        assertEquals(1,(int)steven.getTrainerId());
    }
}
