package be.multimedi.classpokemondb.pokemonspecie;

import be.multimedi.classpokemondb.pokemontype.PokemonType;
import be.multimedi.classpokemondb.pokemontype.PokemonTypeDAO;
import be.multimedi.classpokemondb.trainer.Trainer;

import java.sql.Date;
import java.util.List;

public class PokemonSpecie {
    Integer id;
    String name;
    Integer type1Id;
    Integer type2Id;
    Integer evolvedFromId;

    public PokemonSpecie(Integer id, String name, Integer type1Id, Integer type2Id, Integer evolvedFromId) {
        this.id = id;
        this.name = name;
        this.type1Id = type1Id;
        this.type2Id = type2Id;
        this.evolvedFromId = evolvedFromId;
    }

    public PokemonSpecie(String name, Integer type1Id, Integer type2Id, Integer evolvedFromId) {
        this(-1, name, type1Id, type2Id, evolvedFromId);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null || name.isBlank()) throw new IllegalArgumentException("Name is required!");
        this.name = name;
    }

    public Integer getType1Id() {
        return type1Id;
    }

    private void setType1Id(Integer type1Id) {
        if (type1Id == null) throw new IllegalArgumentException("Type1ID is required!");
        this.type1Id = type1Id;
    }

    public Integer getType2Id() {
        return type2Id;
    }

    private void setType2Id(Integer type2Id) {
        this.type2Id = type2Id;
    }

    public Integer getEvolvedFromId() {

        return evolvedFromId;
    }

    private void setEvolvedFromId(Integer evolvedFromId) {
        this.evolvedFromId = evolvedFromId;
    }

    public static PokemonSpecie getSpeciesById(List<PokemonSpecie> list, int id) {
        for (PokemonSpecie ps : list) {
            if (ps.getId() == id) {
                return ps;
            }
        }
        return null;

    }
}

