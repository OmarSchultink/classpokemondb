package be.multimedi.classpokemondb.pokemonspecie;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static be.multimedi.classpokemondb.db.DriverManagerWrapper.*;

public class PokemonSpecieDAO {

    /*helper function to get a species from the ResultSet of DB*/
    private static PokemonSpecie getPokemonSpeciesByResultSet(ResultSet rs){
        try {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            int type1Id = rs.getInt(3);
            Integer type2Id = rs.getObject(4, Integer.class);
            Integer evolvedFromId = rs.getObject(5, Integer.class);
            try {
                return new PokemonSpecie(id, name, type1Id, type2Id, evolvedFromId);
            } catch (IllegalArgumentException iae) {
                System.out.println("illegal data in PokemonSpecie table: " + iae.getMessage());
            }
        }catch(SQLException se){
            System.out.println("oops: " + se);
        }
        return null;
    }

    public static PokemonSpecie getPokemonSpById(int id) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * from PokemonSP where  id=" + id)) {
            if (rs.next()) {
                return getPokemonSpeciesByResultSet(rs);
            }
        } catch (SQLException se) {
            System.out.println("There is a connnection error " + se);
        }
        return null;
    }

    public static PokemonSpecie getPokemonSpecieByName(String name) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM PokemonSP where naam='" + name + "'");)
        {
            if(rs.next()){
                return getPokemonSpeciesByResultSet(rs);
            }
        } catch (SQLException se) {
            System.out.println("There is a SQL error: " + se);
        }
        return null;
    }



        public static List<PokemonSpecie> getAllPokemonSpecies() {
        List<PokemonSpecie> list = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM PokemonSP");) {
            while (rs.next()) {
                PokemonSpecie ps = getPokemonSpeciesByResultSet(rs);
                if(ps!=null){
                    list.add(ps);
                }
            }
        } catch (SQLException se) {
            System.out.println("There is a SQL error: " + se);

        }
        return list;
    }
}
