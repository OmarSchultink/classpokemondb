package be.multimedi.classpokemondb.pokemontype;

import static be.multimedi.classpokemondb.db.DriverManagerWrapper.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PokemonTypeDAO {

    private static PokemonType getPokemonTypeFromResultSet(ResultSet rs){
        try {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            try {
                return new PokemonType(id, name);
            } catch (IllegalArgumentException iae) {
                System.out.println("Illegal data in table PokemonType in DB: " + iae.getMessage());
            }
        }catch(SQLException iae){
            System.out.println("oops: "+iae);
        }
        return null;
    }

    public static PokemonType getPokemonTypeById(int id) {
        try (   Connection con = DriverManager.getConnection(url, login, pwd);
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(
                        "SELECT * FROM PokemonType WHERE id = " + id))
        {
            if (rs.next()) return getPokemonTypeFromResultSet(rs);

        } catch (SQLException se) {
            System.out.println("Try again " + se);
        }
        return null;
    }

    public static List<PokemonType> getPokemonTypeByName(String name) {
        List<PokemonType> list = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT  * From PokemonType where naam= '" + name + "'");)
        {
            while (rs.next()) {
                PokemonType pt = getPokemonTypeFromResultSet(rs);
                if (pt!=null) list.add(pt);
            }
        } catch (SQLException se) {
            System.out.println("Error!" + se);
        }
        return list;
    }

    public static List<PokemonType> getAllPokemonTypes() {
        List<PokemonType> list = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT  * FROM PokemonType");)
        {
            while (rs.next()) {
                PokemonType pt = getPokemonTypeFromResultSet(rs);
                if (pt != null) list.add(pt);
            }
        }catch (SQLException se) {
            System.out.println("Error: " + se);
        }
        return list;
    }
}