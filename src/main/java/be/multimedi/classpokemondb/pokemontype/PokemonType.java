package be.multimedi.classpokemondb.pokemontype;

import java.sql.Date;

public class PokemonType {
    Integer id;
    String name;

    public PokemonType(Integer id, String name) {
        this.id = id;
        this.name = name;

    }
    public PokemonType (String name){
        this(-1, name);
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.isBlank())
            throw new IllegalArgumentException("Pokemon type name is required. ");
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%02d %-20s", id, name);
    }

}