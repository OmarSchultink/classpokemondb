package be.multimedi.classpokemondb;

import be.multimedi.classpokemondb.consoleInputTool.ConsoleInputTool;
import be.multimedi.classpokemondb.outputUtility.Menus;
import be.multimedi.classpokemondb.outputUtility.Table;
import be.multimedi.classpokemondb.pokemon.Pokemon;
import be.multimedi.classpokemondb.pokemon.PokemonDAO;
import be.multimedi.classpokemondb.pokemonspecie.PokemonSpecieDAO;
import be.multimedi.classpokemondb.pokemonspecie.PokemonSpecie;
import be.multimedi.classpokemondb.pokemontype.PokemonType;
import be.multimedi.classpokemondb.pokemontype.PokemonTypeDAO;
import be.multimedi.classpokemondb.trainer.Trainer;
import be.multimedi.classpokemondb.trainer.TrainerDAO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

/**
 * PokemonApp
 */
public class PokemonApp {

    Menus menus;

    public static void main(String[] args) {
        PokemonApp app = new PokemonApp();
        app.startApp();
    }


    public void startApp() {

        System.out.println("**********Welcome to the POKEMON APP, Professor Oak*********");
        menus = new Menus();
        int choice;
        do {
            menus.printMainMenu();
            choice = ConsoleInputTool.askUserInputInteger(
                    "Your Choice: ", 0, 5);
            switch (choice) {
                case 1:
                    add();
                    break;
                case 2:
                    remove();
                    break;
                case 3:
                    alter();
                    break;
                case 4:
                    show();
                    break;
                case 5:
                    search();
            }

        } while (choice != 0);
        System.out.println("Bye Professor Oak!");
    }

    public void add() {
        System.out.println("What would you like to add, Professor Oak?");
        int choice;
        do {
            menus.printAddMenu();
            choice = ConsoleInputTool.askUserInputInteger("Your choice:", 0, 2);
            switch (choice) {
                case 0:
                    break;
                case 1:
                    addTrainer();
                    break;
                case 2:
                    addPokemon();
                    break;
            }

        } while (choice != 0);

    }

    public void addTrainer() {
        String firstName = ConsoleInputTool.askUserInputString("Add firstname of trainer: ", 1);
        String lastName = ConsoleInputTool.askUserInputString("Add lastname of trainer: ", 1);
        boolean isValidBirthDay = false;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/d");
        LocalDate localDateBirthday = null;
        do {
            String birthday = ConsoleInputTool.askUserInputString("Add birthday of trainer (format YYYY/MM/DD): ", 0);
            try {
                localDateBirthday = LocalDate.parse(birthday, formatter);
                isValidBirthDay = true;
            } catch (DateTimeParseException dtpe) {
                System.out.println("Invalid birthday.");
            }
        } while (!isValidBirthDay);
        String city = ConsoleInputTool.askUserInputString("Add city of trainer: ", 1);

        Trainer trainer1 = new Trainer(firstName, lastName, localDateBirthday, city);
        if (!TrainerDAO.registerTrainer(trainer1)) {
            System.out.println("Could not add trainer.");
        } else {
            System.out.println("Trainer added.");
        }
    }

    public void addPokemon() {
        boolean petQuestion = ConsoleInputTool.askUserYesNoQuestion("Do you want to add a petname for your pokemon (y/n)?");
        String petName = null;
        if (petQuestion == true) {
            petName = ConsoleInputTool.askUserInputString("Enter a petname for you Pokemon", 1);
        }

        showPokemonSpecies();
        int inputSPID = ConsoleInputTool.askUserInputInteger("Choose a species. Enter the corresponding id:", 1, 151);

        boolean trainerquestion = ConsoleInputTool.askUserYesNoQuestion("Do you want to assign a trainer to your pokemon (y/n)?");
        Integer inputTrainer = null;
        if (trainerquestion == true) {
            do {
                showTrainers();
                inputTrainer = ConsoleInputTool.askUserInputInteger("Choose a trainer. Enter the corresponding id:", 1);
                if (Trainer.getTrainerById(TrainerDAO.getAllTrainers(), inputTrainer) != null) break;
                System.out.println(inputTrainer + " is not an existing trainer id. Please try again.");
            } while (true);
        }

        Pokemon poke = new Pokemon(petName, inputSPID, inputTrainer);
        if (PokemonDAO.registerPokemon(poke)) {
            System.out.println("Pokemon added.");
        } else {
            System.out.println("Could not add pokemon.");
        }
    }

    public void remove() {
        System.out.println("What would you like to remove, Professor Oak?");
        int choice;
        do {
            menus.printRemoveMenu();
            choice = ConsoleInputTool.askUserInputInteger("Your choice:", 0, 2);
            switch (choice) {
                case 0:
                    break;
                case 1:
                    removeTrainer();
                    break;
                case 2:
                    removePokemon();
                    break;
            }

        } while (choice != 0);

    }

    public void removeTrainer() {
        int input = -1;
        do {
            showTrainers();
            input = ConsoleInputTool.askUserInputInteger("Enter the TrainerId which you would like to remove, or press 0 to abort.");
            if (Trainer.getTrainerById(TrainerDAO.getAllTrainers(), input) != null || input == 0) {
                break;
            } else {
                System.out.println(input + " is not an existing trainer id. Please try again.");
            }
        } while (input != 0);

        if (input != 0) {
            if (TrainerDAO.removeTrainer(TrainerDAO.getTrainerById(input))) {
                System.out.println("Trainer succesfully removed");
            } else {
                System.out.println("Could not remove trainer!");
            }
        }
    }

    public void removePokemon() {
        List<Pokemon> list = PokemonDAO.getAllPokemon();
        showPokemons();
        int input = -1;
        do {
            input = ConsoleInputTool.askUserInputInteger("Enter the PokemonId which you would like to remove, or press 0 to abort.");
            if (Pokemon.getPokemonById(list, input) != null || input == 0) break;
            else {
                System.out.println(input + " is not an existing pokemon id. Please try again.");
            }
        } while (input != 0);
        if (input != 0) {
            if (PokemonDAO.removePokemon(PokemonDAO.getPokemonById(input))) {
                System.out.println("Pokemon succesfully removed");
            } else {
                System.out.println("Could not remove pokemon!");
            }
        }

    }

    public void alter() {
        System.out.println("What would you like to alter, Professor Oak?");
        int choice;
        do {
            menus.printAlterMenu();
            choice = ConsoleInputTool.askUserInputInteger("Your choice:", 0, 2);
            switch (choice) {
                case 0:
                    break;
                case 1:
                    alterTrainer();
                    break;
                case 2:
                    alterPokemon();
                    break;
            }

        } while (choice != 0);
    }

    public void alterTrainer() {

        System.out.println("Which trainer do you want to alter?");
        List<Trainer> list = TrainerDAO.getAllTrainers();
        showTrainers();
        int trainerId = -1;
        do {
            trainerId = ConsoleInputTool.askUserInputInteger("Enter the TrainerId, or press 0 to abort.");
            if (Trainer.getTrainerById(list, trainerId) != null || trainerId == 0) {
                break;
            } else {
                System.out.println(trainerId + " is not an existing trainer id. Please try again.");
            }
        } while (trainerId != 0);
        if (trainerId != 0) {
            System.out.println("What would you like to alter, professor Oak?");
            int choice;
            do {
                menus.printAlterTrainerMenu();
                choice = ConsoleInputTool.askUserInputInteger("Your choice: ", 0, 4);
                switch (choice) {
                    case 0:
                        break;
                    case 1:
                        String trainerName = ConsoleInputTool.askUserInputString("Enter a new trainer name: ", 1);
                        if (TrainerDAO.alterTrainerFirstName(trainerId, trainerName)) {
                            System.out.println("Trainer altered:");
                            showTrainerByid(trainerId);
                        } else System.out.println("Could not alter Trainer");
                        break;
                    case 2:
                        String trainerLastName = ConsoleInputTool.askUserInputString("Enter a new last name: ", 1);
                        if (TrainerDAO.alterTrainerLastName(trainerId, trainerLastName)) {
                            System.out.println("Trainer altered:");
                            showTrainerByid(trainerId);
                        } else System.out.println("Could not alter Trainer");

                        break;
                    case 3:

                        while (true) {
                            try {
                                String trainerBirthday = ConsoleInputTool.askUserInputString("Enter a new birthdate yyyy/MM/dd: ", 1);
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/d");
                                LocalDate ld = LocalDate.parse(trainerBirthday, formatter);

                                if (TrainerDAO.alterTrainerBirthday(trainerId, ld)) {
                                    System.out.println("Trainer altered:");
                                    showTrainerByid(trainerId);
                                } else System.out.println("Could not alter Trainer");
                                break;
                            } catch (DateTimeParseException dtpe) {
                                System.out.println("Please enter a valid date-format");
                            }
                        }
                        break;
                    case 4:
                        String trainerCity = ConsoleInputTool.askUserInputString("Enter a new city", 1);
                        if (TrainerDAO.alterTrainerCity(trainerId, trainerCity)) {
                            System.out.println("Trainer altered:");
                            showTrainerByid(trainerId);
                        } else System.out.println("Could not alter Trainer");
                        break;
                }
            } while (choice != 0);
        }
    }

    public void alterPokemon() {
        List<Pokemon> list = PokemonDAO.getAllPokemon();
        System.out.println("Which pokemon do you want to alter?");
        showPokemons();
        int pokemonId = -1;
        do {
            pokemonId = ConsoleInputTool.askUserInputInteger("Enter the Pokemon id, or press 0 to abort.");
            if (Pokemon.getPokemonById(list, pokemonId) != null || pokemonId == 0) {
                break;
            } else {
                System.out.println(pokemonId + " is not an existing pokemon id. Please try again.");
            }
        } while (pokemonId != 0);
        System.out.println("What would you like to alter, professor Oak?");
        int choice;
        do {
            menus.printAlterPokemonMenu();
            choice = ConsoleInputTool.askUserInputInteger("Your choice: ", 0, 2);
            switch (choice) {
                case 0:
                    break;
                case 1: // name
                    boolean clearPokemonName = false;
                    if (Pokemon.getPokemonById(list, pokemonId).getName() != null) {
                        clearPokemonName = ConsoleInputTool.askUserYesNoQuestion("Would you like to clear the pokemon's name (y/n)?");
                    }
                    String pokemonName = "";
                    if (clearPokemonName) {
                        pokemonName = null;
                    } else {
                        pokemonName = ConsoleInputTool.askUserInputString("Enter a new pokemon name: ", 1);
                    }
                    if (PokemonDAO.alterPokemonName(pokemonId, pokemonName)) {
                        System.out.println("Pokemon altered:");
                        showPokemonById(pokemonId);
                    } else System.out.println("Could not alter Pokemon");
                    break;
                case 2: //trainer
                    List<Trainer> listTrainer = TrainerDAO.getAllTrainers();
                    boolean clearPokemonTrainer = false;
                    if (Pokemon.getPokemonById(list, pokemonId).getTrainerId() != null) {
                        clearPokemonTrainer = ConsoleInputTool.askUserYesNoQuestion("Would you like to clear the pokemon's trainer (y/n)?");
                    }
                    Integer pokemonTrainer = -1;
                    if (clearPokemonTrainer) {
                        pokemonTrainer = null;
                    } else {
                        do {
                            showTrainers();
                            ;
                            pokemonTrainer = ConsoleInputTool.askUserInputInteger("Enter a new trainer for the pokemon: ");
                            if (Trainer.getTrainerById(listTrainer, pokemonTrainer) == null) {
                                System.out.println(pokemonTrainer + " is not an existing trainer id. Please try again.");
                            } else break;
                        } while (true);
                    }
                    if (PokemonDAO.alterPokemonTrainerId(pokemonId, pokemonTrainer)) {
                        System.out.println("Pokemon altered:");
                        showPokemonById(pokemonId);
                    } else System.out.println("Could not alter Pokemon");
                    break;
            }
        } while (choice != 0);//what would you like to alter switch
    }

    public void show() {
        System.out.println("What would you like to see, Professor Oak?");
        int choice;
        do {
            menus.printShowMenu();
            choice = ConsoleInputTool.askUserInputInteger("Your choice:", 0, 4);
            switch (choice) {
                case 0:
                    break;
                case 1:
                    showTrainers();
                    break;
                case 2:
                    showPokemons();
                    break;
                case 3:
                    showPokemonTypes();
                    break;
                case 4:
                    showPokemonSpecies();
                    break;
            }

        } while (choice != 0);
    }

    public void showTrainers() {
        List<Trainer> trainers = TrainerDAO.getAllTrainers();
        Table trainerTable = new Table("ID","FIRST NAME", "LAST NAME", "BIRTHDAY", "CITY");
        if (trainers.size() > 0) {
            for (Trainer t : trainers) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
                String bDayString = t.getBirthday().format(formatter);
                //System.out.format("%04d %-20s %-20s %s \t %-20s\n", t.getId(), t.getFirstName(), t.getLastName(), bDayString, t.getCity());
                trainerTable.addRow(Integer.toString(t.getId()),t.getFirstName(),t.getLastName(),bDayString,t.getCity());
             }
            trainerTable.print();
        } else {
            System.out.println("List of trainers is empty.");
        }
    }

    public void showTrainerByid(int id) {
        List<Trainer> trainers = TrainerDAO.getAllTrainers();
        Trainer t = Trainer.getTrainerById(trainers, id);
        Table trainerTable = new Table("ID","FIRST NAME", "LAST NAME", "BIRTHDAY", "CITY");
        if (t != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            String bDayString = t.getBirthday().format(formatter);
            trainerTable.addRow(Integer.toString(t.getId()),t.getFirstName(),t.getLastName(),bDayString,t.getCity());
            trainerTable.print();
        }
        else System.out.println("No such trainer found!");
    }

    public void showPokemons() {
        List<Pokemon> poke = PokemonDAO.getAllPokemon();
        List<PokemonSpecie> pokeS = PokemonSpecieDAO.getAllPokemonSpecies();
        List<Trainer> pokeTrainer = TrainerDAO.getAllTrainers();
        Table pokemonTable = new Table("ID","NAME","SPECIES","TRAINER");
        if (poke.size() > 0) {
            for (Pokemon p : poke) {
                String name;
                if (p.getName() == null) name = pokeS.get(p.getSPID() - 1).getName();
                else name = p.getName();

                String trainerName = "";
                if (p.getTrainerId() != null && p.getTrainerId() > 0) {
                    Trainer t = Trainer.getTrainerById(pokeTrainer, p.getTrainerId());
                    if (t != null) trainerName = t.getFirstName();
                }
                pokemonTable.addRow(Integer.toString(p.getId()),name,pokeS.get(p.getSPID() - 1).getName(),trainerName);

            }
            pokemonTable.print();
        } else {
            System.out.println("List of Pokemons is empty.");
        }

    }

    public void showPokemonById(int id) {
        List<Pokemon> poke = PokemonDAO.getAllPokemon();
        List<PokemonSpecie> pokeS = PokemonSpecieDAO.getAllPokemonSpecies();
        List<Trainer> pokeTrainer = TrainerDAO.getAllTrainers();
        Pokemon p = Pokemon.getPokemonById(poke, id);
        Table pokemonTable = new Table("ID","NAME","SPECIES","TRAINER");
        if (p != null) {
            String name;
            if (p.getName() == null) name = pokeS.get(p.getSPID() - 1).getName();
            else name = p.getName();

            String trainerName = "";
            if (p.getTrainerId() != null && p.getTrainerId() > 0) {
                Trainer t = Trainer.getTrainerById(pokeTrainer, p.getTrainerId());
                if (t != null) trainerName = t.getFirstName();
            }
            pokemonTable.addRow(Integer.toString(p.getId()),name,pokeS.get(p.getSPID() - 1).getName(),trainerName);
            pokemonTable.print();
        } else System.out.println("No such pokemon found!");
    }

    public void showPokemonTypes() {
        Table pokemonTypesTable = new Table("ID","NAME");
        for (PokemonType pt : PokemonTypeDAO.getAllPokemonTypes()) {
            pokemonTypesTable.addRow(Integer.toString(pt.getId()),pt.getName());
        }
        pokemonTypesTable.print();
    }

    public void showPokemonSpecies() {
        List<PokemonSpecie> pklist = PokemonSpecieDAO.getAllPokemonSpecies();//buffer pokemonspecies Lijst
        List<PokemonType> ptlist = PokemonTypeDAO.getAllPokemonTypes();// buffer pokemon types list
        Table pokemonSpeciesTable = new Table("ID","SPECIES","TYPE 1", "TYPE 2", "EVOLVED FROM");
        for (PokemonSpecie ps : pklist) {//lees alle pkmn uit
            String evolved = "";
            if (ps.getEvolvedFromId() != null && ps.getEvolvedFromId() > 0) {
                PokemonSpecie psevolved = pklist.get(ps.getEvolvedFromId() - 1);
                if (psevolved != null) {
                    evolved = psevolved.getName();
                }
            }
            String tweedeType = "";
            if (ps.getType2Id() != null && ps.getType2Id() > 0) {
                PokemonType pt = ptlist.get(ps.getType2Id() - 1);
                if (pt != null) {
                    tweedeType = pt.getName();
                }
            }
            pokemonSpeciesTable.addRow(Integer.toString(ps.getId()),ps.getName(),ptlist.get(ps.getType1Id()-1).getName(),tweedeType,evolved);
        }
        pokemonSpeciesTable.print();
    }

    public void search() {
        System.out.println("What would you like to search, Professor Oak?");
        int choice;
        do {
            menus.printSearchMenu();
            choice = ConsoleInputTool.askUserInputInteger("Your choice:", 0, 4);
            switch (choice) {
                case 0:
                    break;
                case 1:
                    searchTrainers();
                    break;
                case 2:
                    searchPokemons();
                    break;
                case 3:
                    searchPokemonTypes();
                    break;
                case 4:
                    searchPokemonSpecies();
                    break;
            }

        } while (choice != 0);
    }

    public void searchTrainers() {
        System.out.println("Search trainers by:");
        int choice;
        System.out.println("1) firstname");
        System.out.println("2) id");
        choice = ConsoleInputTool.askUserInputInteger("Your choice:", 1, 2);
        switch (choice) {
            case 1:
                searchTrainersByFirstName();
                break;
            case 2:
                searchTrainerById();
                break;
        }
    }

    public void searchPokemons() {
        System.out.println("Search pokemons by:");
        int choice;
        System.out.println("1) name");
        System.out.println("2) id");
        choice = ConsoleInputTool.askUserInputInteger("Your choice:", 1, 2);
        switch (choice) {
            case 1:
                searchPokemonsByName();
                break;
            case 2:
                searchPokemonById();
                break;
        }

    }

    public void searchPokemonTypes() {
        System.out.println("Search pokemontypes by:");
        int choice;
        System.out.println("1) name");
        System.out.println("2) id");
        choice = ConsoleInputTool.askUserInputInteger("Your choice:", 1, 2);
        switch (choice) {
            case 1:
                searchPokemonTypesByName();
                break;
            case 2:
                searchPokemonTypesById();
                break;
        }
    }

    public void searchPokemonSpecies() {
        System.out.println("Search pokemonspecies by:");
        int choice;
        System.out.println("1) name");
        System.out.println("2) id");
        choice = ConsoleInputTool.askUserInputInteger("Your choice:", 1, 2);
        switch (choice) {
            case 1:
                searchPokemonSpeciesByName();
                break;
            case 2:
                searchPokemonSpeciesById();
                break;
        }
    }

    public void searchTrainersByFirstName() {
        String fn = ConsoleInputTool.askUserInputString("Firstname of Trainer: ", 1);
        List<Trainer> trainerList = TrainerDAO.getTrainersByFirstName(fn);
        List<Trainer> allTrainers = TrainerDAO.getAllTrainers();
        if (trainerList.size() == 0) {
            System.out.println("No trainers found");
        } else {
            int count=0;
            System.out.println("----------------------------------------------------------------------------");
            System.out.format("%-4s %-4s %-20s %-20s %-10s  \t %-20s\n","line", "id", "First name", "Last name", "Birthday", "City");
            System.out.println("----------------------------------------------------------------------------");
            for (Trainer t : trainerList) {
                count++;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
                String bDayString = t.getBirthday().format(formatter);
                System.out.format("%04d %04d %-20s %-20s %s \t\t %-20s\n",count, t.getId(), t.getFirstName(), t.getLastName(), bDayString, t.getCity());
            }
            boolean makeChange=ConsoleInputTool.askUserYesNoQuestion("Would you like to make changes(y/n)?");
            if (makeChange){
                int changeline=ConsoleInputTool.askUserInputInteger("Which line would you like to alter?",1,count);
                int trainerToChangeId = trainerList.get(changeline-1).getId();
                System.out.println("What would you like to alter?");
                int choice;
                do {
                    menus.printAlterTrainerMenu();
                    choice = ConsoleInputTool.askUserInputInteger("Your choice: ", 0, 4);
                    switch (choice) {
                        case 0:
                            break;
                        case 1:
                            String trainerName = ConsoleInputTool.askUserInputString("Enter a new trainer name: ", 1);
                            if (TrainerDAO.alterTrainerFirstName(trainerToChangeId, trainerName)) {
                                System.out.println("Trainer altered:");
                                showTrainerByid(trainerToChangeId);
                            } else System.out.println("Could not alter Trainer");
                            break;
                        case 2:
                            String trainerLastName = ConsoleInputTool.askUserInputString("Enter a new last name: ", 1);
                            if (TrainerDAO.alterTrainerLastName(trainerToChangeId, trainerLastName)) {
                                System.out.println("Trainer altered:");
                                showTrainerByid(trainerToChangeId);
                            } else System.out.println("Could not alter Trainer");

                            break;
                        case 3:
                            while (true) {
                                try {
                                    String trainerBirthday = ConsoleInputTool.askUserInputString("Enter a new birthdate yyyy/MM/dd: ", 1);
                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/d");
                                    LocalDate ld = LocalDate.parse(trainerBirthday, formatter);

                                    if (TrainerDAO.alterTrainerBirthday(trainerToChangeId, ld)) {
                                        System.out.println("Trainer altered:");
                                        showTrainerByid(trainerToChangeId);
                                    } else System.out.println("Could not alter Trainer");
                                    break;
                                } catch (DateTimeParseException dtpe) {
                                    System.out.println("Please enter a valid date-format");
                                }
                            }
                            break;
                        case 4:
                            String trainerCity = ConsoleInputTool.askUserInputString("Enter a new city", 1);
                            if (TrainerDAO.alterTrainerCity(trainerToChangeId, trainerCity)) {
                                System.out.println("Trainer altered:");
                                showTrainerByid(trainerToChangeId);
                            } else System.out.println("Could not alter Trainer");
                            break;
                    }
                } while (choice != 0);
            }//if makechange

        }//else
    }

    public void searchPokemonSpeciesById() {
        List<PokemonSpecie> pklist = PokemonSpecieDAO.getAllPokemonSpecies();//buffer pokemonspecies Lijst
        List<PokemonType> ptlist = PokemonTypeDAO.getAllPokemonTypes();// buffer pokemon types list
        int in = ConsoleInputTool.askUserInputInteger("Id of pokemon species: ", 1);
        PokemonSpecie ps = PokemonSpecieDAO.getPokemonSpById(in);
        if (ps != null) {
            System.out.format("%-4s %-15s %-10s %-10s %-4s\n", "Id", "Species", "Type 1", "Type 2", "Evolved from");//header
            String evolved = "";
            if (ps.getEvolvedFromId() != null && ps.getEvolvedFromId() > 0) {
                PokemonSpecie psevolved = pklist.get(ps.getEvolvedFromId() - 1);
                if (psevolved != null) {
                    evolved = psevolved.getName();
                }
            }
            String tweedeType = "";
            if (ps.getType2Id() != null && ps.getType2Id() > 0) {
                PokemonType pt = ptlist.get(ps.getType2Id() - 1);
                if (pt != null) {
                    tweedeType = pt.getName();
                }
            }
            System.out.format("%04d %-15s %-10s %-10s %-20s\n", ps.getId(), ps.getName(), ptlist.get(ps.getType1Id() - 1).getName(), tweedeType, evolved);
        } else System.out.println("No such pokemon species found!");
    }

    public void searchTrainerById() {
        Integer id = ConsoleInputTool.askUserInputInteger("Enter a trainer ID number");
        showTrainerByid(id);
    }

    public void searchPokemonsByName() {
        String nm = ConsoleInputTool.askUserInputString("Enter a Pokemon name: ", 1);
        List<PokemonSpecie> pokeS = PokemonSpecieDAO.getAllPokemonSpecies();
        List<Pokemon>allPokemon=PokemonDAO.getAllPokemon();
        List<Trainer> pokeTrainer = TrainerDAO.getAllTrainers();
        List<Pokemon> nameList = PokemonDAO.getPokemonByName(nm);
        if (nameList.size() == 0) {
            System.out.println("No Pokemons found");
        } else {
            System.out.println("------------------------------------------------------------");
            System.out.format("%-4s %-4s %-15s %-15s \t%-15s\n","line", "id", "Name", "Species ID", "Trainer ID");
            System.out.println("------------------------------------------------------------");
            int count=0;
            for (Pokemon p : nameList) {
                count++;
                String name;
                if (p.getName() == null) name = pokeS.get(p.getSPID() - 1).getName();
                else name = p.getName();

                String trainerName = "";
                if (p.getTrainerId() != null && p.getTrainerId() > 0) {
                    Trainer t = Trainer.getTrainerById(pokeTrainer, p.getTrainerId());
                    if (t != null) trainerName = t.getFirstName();
                }
                System.out.format("%04d %04d %-15s %-15s \t%-15s\n",
                        count,
                        p.getId(),
                        name,
                        pokeS.get(p.getSPID() - 1).getName(),
                        trainerName);
            }
            boolean makeChange=ConsoleInputTool.askUserYesNoQuestion("Would you like to make changes(y/n)?");
            if (makeChange){
                int changeline=ConsoleInputTool.askUserInputInteger("Which line would you like to alter?",1,count);
                int pokemonToChangeId = nameList.get(changeline-1).getId();
                System.out.println("What would you like to alter?");
                int choice;
                do {
                    menus.printAlterPokemonMenu();
                    choice = ConsoleInputTool.askUserInputInteger("Your choice: ", 0, 2);
                    switch (choice) {
                        case 0:
                            break;
                        case 1: // name
                            boolean clearPokemonName = false;
                            if (Pokemon.getPokemonById(allPokemon, pokemonToChangeId).getName() != null) {
                                clearPokemonName = ConsoleInputTool.askUserYesNoQuestion("Would you like to clear the pokemon's name (y/n)?");
                            }
                            String pokemonName = "";
                            if (clearPokemonName) {
                                pokemonName = null;
                            } else {
                                pokemonName = ConsoleInputTool.askUserInputString("Enter a new pokemon name: ", 1);
                            }
                            if (PokemonDAO.alterPokemonName(pokemonToChangeId, pokemonName)) {
                                System.out.println("Pokemon altered:");
                                showPokemonById(pokemonToChangeId);
                            } else System.out.println("Could not alter Pokemon");
                            break;
                        case 2: //trainer
                            List<Trainer> listTrainer = TrainerDAO.getAllTrainers();
                            boolean clearPokemonTrainer = false;
                            if (Pokemon.getPokemonById(allPokemon, pokemonToChangeId).getTrainerId() != null) {
                                clearPokemonTrainer = ConsoleInputTool.askUserYesNoQuestion("Would you like to clear the pokemon's trainer (y/n)?");
                            }
                            Integer pokemonTrainer = -1;
                            if (clearPokemonTrainer) {
                                pokemonTrainer = null;
                            } else {
                                do {
                                    showTrainers();
                                    ;
                                    pokemonTrainer = ConsoleInputTool.askUserInputInteger("Enter a new trainer for the pokemon: ");
                                    if (Trainer.getTrainerById(listTrainer, pokemonTrainer) == null) {
                                        System.out.println(pokemonTrainer + " is not an existing trainer id. Please try again.");
                                    } else break;
                                } while (true);
                            }
                            if (PokemonDAO.alterPokemonTrainerId(pokemonToChangeId, pokemonTrainer)) {
                                System.out.println("Pokemon altered:");
                                showPokemonById(pokemonToChangeId);
                            } else System.out.println("Could not alter Pokemon");
                            break;
                    }//switch
                }while(choice!=0);
            }// if makeChange
        }
    }

    public void searchPokemonById() {
        Integer nm = ConsoleInputTool.askUserInputInteger("Enter a Pokemon ID number: ");
        showPokemonById(nm);
    }

    public void searchPokemonTypesByName() {
        String nm = ConsoleInputTool.askUserInputString("Enter a Pokemon type: ", 1);
        List<PokemonType> idList = PokemonTypeDAO.getPokemonTypeByName(nm);
        if (idList.size() == 0) {
            System.out.println("No Pokemon types found");
        } else {
            System.out.println("----------------------------------------------------------------------------");
            System.out.format("%-2s %-20s\n", "id", "Name");
            System.out.println("----------------------------------------------------------------------------");
            for (PokemonType pt : idList) {
                System.out.println(pt.toString());
            }
        }
    }

    public void searchPokemonTypesById() {
        Integer nm = ConsoleInputTool.askUserInputInteger("Enter a Pokemon type ID number: ");
        PokemonType p = PokemonTypeDAO.getPokemonTypeById(nm);
        if (p != null) {
            System.out.println("----------------------------------------------------------------------------");
            System.out.format("%-2s %-20s\n", "id", "Name");
            System.out.println("----------------------------------------------------------------------------");
            System.out.println(p.toString());
        } else System.out.println("No such pokemon type found!");
    }

    public void searchPokemonSpeciesByName() {
        String nm = ConsoleInputTool.askUserInputString("Enter a Pokemon species name: ", 1);
        PokemonSpecie ps = PokemonSpecieDAO.getPokemonSpecieByName(nm);
        List<PokemonSpecie> pklist = PokemonSpecieDAO.getAllPokemonSpecies();//buffer pokemonspecies Lijst
        List<PokemonType> ptlist = PokemonTypeDAO.getAllPokemonTypes();// buffer pokemon types list
        if (ps != null) {
            System.out.println("------------------------------------------------------------");
            System.out.format("%-4s %-15s %-10s %-10s %-4s\n", "Id", "Species", "Type 1", "Type 2", "Evolved from");//header
            System.out.println("------------------------------------------------------------");
            for (PokemonSpecie ps2 : pklist) {//lees alle pkmn uit
                String evolved = "";
                if (ps2.getEvolvedFromId() != null && ps2.getEvolvedFromId() > 0) {
                    PokemonSpecie psevolved = PokemonSpecie.getSpeciesById(pklist,ps2.getEvolvedFromId());
                    if (psevolved != null) {
                        evolved = psevolved.getName();
                    }
                }
                String tweedeType = "";
                if (ps.getType2Id() != null && ps.getType2Id() > 0) {
                    PokemonType pt = ptlist.get(ps.getType2Id() - 1);
                    if (pt != null) {
                        tweedeType = pt.getName();
                    }
                }
                System.out.format("%04d %-15s %-10s %-10s %-20s\n", ps.getId(), ps.getName(), ptlist.get(ps.getType1Id() - 1).getName(), tweedeType, evolved);
            }
        }else System.out.println("No such pokemon species found!");
    }
}
