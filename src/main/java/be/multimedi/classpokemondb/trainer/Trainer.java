package be.multimedi.classpokemondb.trainer;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class Trainer {
    int id;
    String firstName;
    String lastName;
    LocalDate birthday;
    String city;


    public Trainer(Integer id, String firstName, String lastName, LocalDate birthday, String city) {
        this.id = id;
        setFirstName(firstName);
        setLastName(lastName);
        setBirthday(birthday);
        setCity(city);
    }

    public Trainer(String firstName, String lastName, LocalDate birthday, String city) {
        this(-1, firstName, lastName, birthday, city);
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName == null || firstName.isBlank()) throw new IllegalArgumentException("First name is required");
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName == null || lastName.isBlank()) throw new IllegalArgumentException("Last name is required");
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        if (birthday == null) throw new IllegalArgumentException("Birthday is required");
        this.birthday = birthday;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        if (city == null || city.isBlank()) throw new IllegalArgumentException("City is required");
        this.city = city;
    }

    @Override
    public String toString() {
        return String.format("%04d %-20s %-30s %tD \t\t %-20s ", id,firstName,lastName, Date.valueOf(birthday),city);
    }

    public static Trainer getTrainerById(List<Trainer> list, int id){
        for(Trainer t: list){
            if(t.getId()==id){
                return t;
            }
        }
        return null;
    }
}
