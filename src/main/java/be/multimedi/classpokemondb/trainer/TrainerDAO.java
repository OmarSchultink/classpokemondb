package be.multimedi.classpokemondb.trainer;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static be.multimedi.classpokemondb.db.DriverManagerWrapper.*;

public class TrainerDAO {

    private static Trainer getTrainerFromResultSet(ResultSet rs){
        try {
            int id = rs.getInt(1);
            String fn = rs.getString(2);
            String ln = rs.getString(3);
            LocalDate bd = rs.getDate(4).toLocalDate();
            String city = rs.getString(5);

            try{
                return new Trainer(id,fn,ln,bd,city);
            }
            catch(IllegalArgumentException iae){
                System.out.println("Illegal data in Trainer table of DB: " + iae.getMessage());
            }
        }catch(SQLException se){
            System.out.println("oops : " + se);
        }
        return null;
    }
    public static Trainer getTrainerById(Integer trainerId) {

        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM Trainer WHERE id = " + trainerId))
        {
            if (rs.next()) return getTrainerFromResultSet(rs);
        } catch (SQLException se) {
            System.out.println("oops : " + se);
        }
        return null;
    }

    public static List<Trainer> getTrainersByFirstName(String name) {

        List<Trainer> list = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM Trainer WHERE firstName = '" + name + "'"))
        {
            while (rs.next()) {
                Trainer t = getTrainerFromResultSet(rs);
                if(t!=null) list.add(t);
            }
        } catch (SQLException se) {
            System.out.println("oops : " + se);
        }
        return list;
    }

    public static List<Trainer> getAllTrainers() {
        List<Trainer> list = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM Trainer");)
        {
            while (rs.next()) {
                Trainer t = getTrainerFromResultSet(rs);
                if(t!=null) list.add(t);
            }
        } catch (SQLException se) {
            System.out.println("oops : " + se);
        }
        return list;
    }

    public static boolean registerTrainer(Trainer trainer) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt = con.prepareStatement(
                     "INSERT INTO Trainer (firstName, lastName, birthday, city) VALUES (?,?,?,?)");
        ) {
            stmt.setString(1, trainer.getFirstName());
            stmt.setString(2, trainer.getLastName());
            stmt.setDate(3, Date.valueOf(trainer.getBirthday()));
            stmt.setString(4, trainer.getCity());
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException se) {
            System.out.println("oops: " + se);
        }
        return false;
    }

    public static boolean removeTrainer(Trainer trainer) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt = con.prepareStatement("DELETE FROM Trainer WHERE id = ?");
        ) {
            stmt.setInt(1, trainer.getId());
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException se) {
            System.out.println("oops: " + se);
        } catch (NullPointerException npe) {
            System.err.println("No trainer found with this ID");
        }
        return false;
    }

    public static boolean alterTrainerFirstName(int id, String newName) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt = con.prepareStatement("UPDATE Trainer SET firstName = ? WHERE id = ?");) {
            stmt.setString(1, newName);
            stmt.setInt(2, id);
            if (stmt.executeUpdate() >0) return true;
        } catch (SQLException se) {
            System.out.println("oops: " + se);
        }
        return false;
    }


    public static boolean alterTrainerLastName(int id, String newName) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt = con.prepareStatement("UPDATE Trainer SET lastName = ? WHERE id = ?");) {
            stmt.setString(1, newName);
            stmt.setInt(2, id);
            if(stmt.executeUpdate()>0) return true;
        } catch (SQLException se) {
            System.out.println("oops: " + se);
        }
        return false;
    }

    public static boolean alterTrainerBirthday(int id, LocalDate birthday) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt = con.prepareStatement("UPDATE Trainer SET birthday = ? WHERE id = ?");) {
            stmt.setDate(1, Date.valueOf(birthday));
            stmt.setInt(2, id);
            if(stmt.executeUpdate()>0) return true;
        } catch (SQLException se) {
            System.out.println("oops: " + se);
        }
        return false;

    }

    public static boolean alterTrainerCity(int id, String city) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt = con.prepareStatement("UPDATE Trainer SET city = ? WHERE id = ?");) {
            stmt.setString(1, city);
            stmt.setInt(2, id);
            if(stmt.executeUpdate()>0) return true;
        } catch (SQLException se) {
            System.out.println("oops: " + se);
        }
        return false;
    }
}


