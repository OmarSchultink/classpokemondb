package be.multimedi.classpokemondb.pokemon;

import java.util.List;

public class Pokemon {

    Integer id;
    String name;
    Integer SPID;
    Integer trainerId;


    public Pokemon(String name, Integer SPID, Integer trainerId) {
        this.name = name;
        this.SPID = SPID;
        this.trainerId = trainerId;
    }

    public Pokemon(Integer id, String name, Integer SPID, Integer trainerId) {
        this.id = id;
        this.name = name;
        this.SPID = SPID;
        this.trainerId = trainerId;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
    }

    public Integer getSPID() {
        return SPID;
    }

    public void setSPID(Integer SPID) {
        this.SPID = SPID;
    }

    public Integer getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(Integer trainerId) {
        this.trainerId = trainerId;
    }

    public static Pokemon getPokemonById(List<Pokemon> list, int id){
        for(Pokemon p: list){
            if(p.getId()==id){
                return p;
            }
        }
        return null;
    }


}
