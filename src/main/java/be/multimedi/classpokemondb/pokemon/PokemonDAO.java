package be.multimedi.classpokemondb.pokemon;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static be.multimedi.classpokemondb.db.DriverManagerWrapper.*;

public class PokemonDAO {


    /*Helper function to retrieve a Pokemon from a ResultSet of the DB*/
    private static Pokemon getPokemonFromResultSet(ResultSet rs){
        try {
            int pokemonId = rs.getInt(1);
            String pokemonName = rs.getString(2);
            int speciesId = rs.getInt(3);
            Integer trainerId = rs.getObject(4, Integer.class);
            try {
                return new Pokemon(pokemonId, pokemonName, speciesId, trainerId);
            } catch (IllegalArgumentException iae) {
                System.out.println("Illegal data in Pokemon table: " + iae.getMessage());
            }
        }catch(SQLException se){
            System.out.println("oops: " + se);
        }
        return null;
    }

    public static Pokemon getPokemonById(int id) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM Pokemon WHERE id = " + id))
        {
            if (rs.next()) {

                return getPokemonFromResultSet(rs);
            }
        } catch (SQLException se) {
            System.out.println("oops : " + se);
        }
        return null;
    }

    public static List<Pokemon> getPokemonByName(String name) {
        List<Pokemon> list = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM Pokemon WHERE naam = '" + name + "'");) {
            while (rs.next()) {
                Pokemon p = getPokemonFromResultSet(rs);
                if(p!=null) list.add(getPokemonFromResultSet(rs));

            }//--> while(rs.next())
        } catch (SQLException se) {
            System.out.println("oops: " + se);
        }
        return list;
    }

    public static List<Pokemon> getAllPokemon() {
        List<Pokemon> list = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM Pokemon");) {
            while (rs.next()) {

                Pokemon p = getPokemonFromResultSet(rs);
                if(p!=null) list.add(getPokemonFromResultSet(rs));
            }//--> while(rs.next())
        } catch (SQLException se) {
            System.out.println("oops: " + se);
        }
        return list;
    }

    public static boolean registerPokemon(Pokemon pokemon) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt =
                     con.prepareStatement("INSERT INTO Pokemon (naam, SpId, trainerId) VALUES (?,?,?)");
        ) {
            stmt.setString(1, pokemon.getName());
            stmt.setInt(2, pokemon.getSPID());
            stmt.setObject(3,pokemon.getTrainerId(),Types.INTEGER);
            if (stmt.executeUpdate() > 0) return true;
        } catch (SQLException se) {
            System.out.println("oops: " + se);
        }
        return false;
    }

    public static boolean removePokemon(Pokemon pokemon) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt = con.prepareStatement("DELETE FROM Pokemon WHERE id = ?");
        ) {
            stmt.setInt(1, pokemon.getId());
            if (stmt.executeUpdate() > 0) return true;
        } catch (SQLException se) {
            System.out.println("oops: " + se);
        } catch (NullPointerException npe) {
            System.err.println("No Pokemon found on this ID");
        }
        return false;
    }

    public static boolean alterPokemonName(int id, String newName) {
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt = con.prepareStatement("UPDATE Pokemon SET naam = ? WHERE id = ?");
        ) {
            stmt.setString(1, newName);
            stmt.setInt(2, id);
            if (stmt.executeUpdate() > 0) return true;
        } catch (SQLException se) {

        }
        return false;
    }

    public static boolean alterPokemonTrainerId(int id, Integer newTrainerId){
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt = con.prepareStatement("UPDATE Pokemon SET trainerId = ? WHERE id = ?");
        ) {
            stmt.setObject(1,newTrainerId, Types.INTEGER);
            stmt.setInt(2,id);
            if (stmt.executeUpdate() > 0) return true;
        }catch(SQLException se){
            System.out.println("oops: " + se);
        }
        return false;
    }


    public static boolean alterPokemonSPId(int id, int newSpeciesId){
        try (Connection con = DriverManager.getConnection(url, login, pwd);
             PreparedStatement stmt = con.prepareStatement("UPDATE Pokemon SET SpId = ? WHERE id = ?");
        ) {
            stmt.setInt(1,newSpeciesId);
            stmt.setInt(2,id);
            if (stmt.executeUpdate() > 0) return true;
        }catch(SQLException se){

            System.out.println("oops: " + se);
        }
        return false;
    }
}
