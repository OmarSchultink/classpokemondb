package be.multimedi.classpokemondb.outputUtility;

import be.multimedi.classpokemondb.pokemon.Pokemon;

public class Table {

    private CommandLineTable table;

    public Table(String... headers){
        this.table = new CommandLineTable();
        table.setShowVerticalLines(true);
        table.setHeaders(headers);
    }

    public void addRow(String... cells){
        table.addRow(cells);
    }

    public void print(){
        table.print();
    }
}
