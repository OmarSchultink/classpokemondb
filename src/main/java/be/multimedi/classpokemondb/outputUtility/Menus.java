package be.multimedi.classpokemondb.outputUtility;

public class Menus {
    private CommandLineTable mainMenu;

    private CommandLineTable addMenu;
    private CommandLineTable removeMenu;
    private CommandLineTable alterMenu;
    private CommandLineTable showMenu;
    private CommandLineTable searchMenu;

    private CommandLineTable alterTrainerMenu;
    private CommandLineTable alterPokemonMenu;

    public Menus(){
        createMainMenu();
        createAddMenu();
        createRemoveMenu();
        createAlterMenu();
        createShowMenu();
        createSearchMenu();
        createAlterTrainerMenu();
        createAlterPokemonMenu();
    }

    private void createMainMenu(){
        mainMenu = new CommandLineTable();
        mainMenu.setShowVerticalLines(true);
        mainMenu.setHeaders("POKEMON MAIN MENU");
        mainMenu.addRow("0) Exit Program");
        mainMenu.addRow("1) Add ... ");
        mainMenu.addRow("2) Remove ...");
        mainMenu.addRow("3) Alter ...");
        mainMenu.addRow("4) Show ...");
        mainMenu.addRow("5) Search ...");
    }

    private void createAddMenu(){
        addMenu = new CommandLineTable();
        addMenu.setShowVerticalLines(true);
        addMenu.setHeaders("ADD MENU");
        addMenu.addRow("0) Go back to MAIN MENU");
        addMenu.addRow("1) Add trainer ...");
        addMenu.addRow("2) Add pokemon ...");
    }

    private void createRemoveMenu(){
        removeMenu = new CommandLineTable();
        removeMenu.setShowVerticalLines(true);
        removeMenu.setHeaders("REMOVE MENU");
        removeMenu.addRow("0) Go back to MAIN MENU");
        removeMenu.addRow("1) Remove trainer ...");
        removeMenu.addRow("2) Remove pokemon ...");
    }

    private void createAlterMenu(){
        alterMenu = new CommandLineTable();
        alterMenu.setShowVerticalLines(true);
        alterMenu.setHeaders("ALTER MENU");
        alterMenu.addRow("0) Go back to MAIN MENU ");
        alterMenu.addRow("1) Alter trainer ...");
        alterMenu.addRow("2) Alter pokemon ...");
    }

    private void createShowMenu(){
        showMenu = new CommandLineTable();
        showMenu.setShowVerticalLines(true);
        showMenu.setHeaders("SHOW MENU");
        showMenu.addRow("0) Go back to MAIN MENU");
        showMenu.addRow("1) Show trainers ...");
        showMenu.addRow("2) Show pokemons ...");
        showMenu.addRow("3) Show pokemon types ...");
        showMenu.addRow("4) Show pokemon species ...");
    }

    private void createSearchMenu(){
        searchMenu = new CommandLineTable();
        searchMenu.setShowVerticalLines(true);
        searchMenu.setHeaders("SEARCH MENU");
        searchMenu.addRow("0) Go back to MAIN MENU");
        searchMenu.addRow("1) Search trainers ...");
        searchMenu.addRow("2) Search pokemons ....");
        searchMenu.addRow("3) Search pokemon types ...");
        searchMenu.addRow("4) Search pokemon species ....");
    }

    private void createAlterTrainerMenu(){
        alterTrainerMenu = new CommandLineTable();
        alterTrainerMenu.setShowVerticalLines(true);
        alterTrainerMenu.setHeaders("ALTER TRAINER MENU");
        alterTrainerMenu.addRow("0) Go back");
        alterTrainerMenu.addRow("1) first name...");
        alterTrainerMenu.addRow("2) last name...");
        alterTrainerMenu.addRow("3) birthday...");
        alterTrainerMenu.addRow("4) city...");
    }

    private void createAlterPokemonMenu(){
        alterPokemonMenu = new CommandLineTable();
        alterPokemonMenu.setShowVerticalLines(true);
        alterPokemonMenu.setHeaders("ALTER POKEMON MENU");
        alterPokemonMenu.addRow("0) Go back");
        alterPokemonMenu.addRow("1) name... ");
        alterPokemonMenu.addRow("2) trainer...");
    }

    public void printMainMenu(){
        mainMenu.print();
    }

    public void printAddMenu(){
        addMenu.print();
    }

    public void printRemoveMenu(){
        removeMenu.print();
    }

    public void printAlterMenu(){
        alterMenu.print();
    }

    public void printShowMenu(){
        showMenu.print();
    }

    public void printSearchMenu(){
        searchMenu.print();
    }

    public void printAlterTrainerMenu(){
        alterTrainerMenu.print();
    }

    public void printAlterPokemonMenu(){
        alterPokemonMenu.print();
    }

}
